### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 5ec282bd-1cb6-441c-9c34-6c2bade29ea8
using SyS

# ╔═╡ a0ee712a-b2a8-11eb-0b03-13eb5bf769ca
"""
	T(;n₀, y₀)	

Devuelve un sistema definido por la ec dif \$y(n) = x(n) + 0.75 * y(n-1)\$, con la condición de contorno \$y(n₀) = y₀\$.

El sistema devuelto es una función que recibe una señal y devuelve otra señal.
"""
T(;n₀, y₀) = x -> function y(n)
	n == n₀ && return y₀
	
	n > n₀ && return x(n) + 0.75 * y(n - 1) 
	
	(y(n + 1) - x(n + 1)) / 0.75 # caso n < n0
end;

# ╔═╡ e16dd150-096e-42be-b90c-aed69b2fb744
begin # Los sistemas
	T₁ = T(n₀=-1, y₀=0)
	T₂ = T(n₀=1, y₀=0)
	T₃ = T(n₀=0, y₀=0)
	T₄ = T(n₀=0, y₀=1)
end;

# ╔═╡ cef13996-57ea-4460-b770-10d94e7f2010
begin # Las respuestas al impulso
	h₁ = T₁(δ)
	h₂ = T₂(δ)
	h₃ = T₃(δ)
	h₄ = T₄(δ)
end;

# ╔═╡ 1c5612e3-b80f-49f6-a299-9c1ed139dc11
let
	ns = -10:10
	
	plot([ stem(ns, h.(ns); xlabel="n") for h in (h₁, h₂, h₃, h₄)]...)
end

# ╔═╡ Cell order:
# ╠═5ec282bd-1cb6-441c-9c34-6c2bade29ea8
# ╠═a0ee712a-b2a8-11eb-0b03-13eb5bf769ca
# ╠═e16dd150-096e-42be-b90c-aed69b2fb744
# ╠═cef13996-57ea-4460-b770-10d94e7f2010
# ╠═1c5612e3-b80f-49f6-a299-9c1ed139dc11
