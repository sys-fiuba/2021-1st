### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 61e633d3-1809-4ccf-84cd-c128d7d1970d
using SyS

# ╔═╡ 45bde18e-a225-11eb-351d-5b6b4643fbe3
md"""
# Serie de Fourier
"""

# ╔═╡ 4b3cecc4-e563-4a0c-901a-d059d0bfc04b
md"""
$$a_k = \frac 1 T \int_0^T x(t) e^{-j \frac{2\pi}{T} k t} dt$$
"""

# ╔═╡ e3c60873-65d9-4bb6-a511-779060ba70cc
@bind kmax Slider(1:50; default=4, show_value=true)

# ╔═╡ a80a0e49-b3e3-4529-afc0-17197c477580
begin 
	a(k) = sinc(k/2) / 2
	squarewave(t) = mod(t + 0.25, 1) < 0.5
	
	plot(squarewave, -1, 2; legend=false)
	plot!(t -> sum(a(k) * exp(im * 2π * k * t) for k in -kmax:kmax) |> real, 
		-1, 2)
end

# ╔═╡ Cell order:
# ╠═45bde18e-a225-11eb-351d-5b6b4643fbe3
# ╠═61e633d3-1809-4ccf-84cd-c128d7d1970d
# ╠═4b3cecc4-e563-4a0c-901a-d059d0bfc04b
# ╟─e3c60873-65d9-4bb6-a511-779060ba70cc
# ╠═a80a0e49-b3e3-4529-afc0-17197c477580
