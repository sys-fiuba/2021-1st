### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 51a38d90-c88c-11eb-3e4f-f59028e39b3e
using SyS

# ╔═╡ ca37313c-0e4d-4bcb-b436-f7970b92188e
using BenchmarkTools

# ╔═╡ 7a139c7b-6522-43d7-b517-6a9b49634366
x(n) = u(n) - u(n-7)

# ╔═╡ fd76c0e0-8b14-4814-9eeb-75e89ecea3b6
begin
	xdesp(k) = n -> x(n - k)
	ns = -20:20
	stem(ns, x.(ns))
	
	stem!(ns, 1 .* xdesp(10).(ns))
	stem!(ns, 1 .* xdesp(-20).(ns))
	stem!(ns, 0 .* xdesp(-10).(ns))
end

# ╔═╡ dd0c98fa-d11d-41c4-9a96-139e2214b476
[1, 2, 3, 4, 5, 6, 7, 8]
[1, 2, 3, 4, 5, 6]
[7, 8, 0, 0, 0, 0]

# ╔═╡ c51feb27-1dbe-4a3d-9589-3b65e2e55759
md"""
N = $(@bind N Slider(7:300; show_value=true))
"""

# ╔═╡ 57b65e66-3b46-413a-8aac-0a1cadeda079
xs = x.(range(0; length=N, step=1))

# ╔═╡ f9419f95-e130-4dd6-985b-4c300e0cea7c
xfs = fft(xs)

# ╔═╡ eb5d1e48-2008-4dd2-87b2-7fe13ceaf642
with(:plotly) do
	#frecs = range(-π; length=N, stop=π)
	frecs = range(0; length=N, step=2π/N)
	pl1 = stem(
		frecs,
		abs.(xfs);
		xlabel="X(Ω)"
		)
	pl2 = stem(
		frecs,
		angle.(xfs);
		xlabel="X(Ω)"
		)
	plot(pl1, pl2; layout=(2, 1))
end

# ╔═╡ 84137336-6e29-406d-a0d6-7b19850f0db7
fft([2, 1, 1, 1, 1, 1])

# ╔═╡ 987ba14e-c430-4855-ac4f-1d11fc8c17ee
[0, 2π/N, 2 * 2π/N, ..., (N-1)/N 2π ]

# ╔═╡ ddf80887-0436-4cdf-8852-c638dea52608
- (-2.5 - 2.45) / (0.85 - (-0.81))

# ╔═╡ 4ebdca91-f592-4278-9481-3c910d00258d
fft([1, 0, 0, 0, 0, 0, 0, 0])

# ╔═╡ cea79b39-8922-467b-834d-a60c6f106bda
[1, 2, 3, 4, 5, 6]
[6, 1, 2, 3, 4, 5]

# ╔═╡ c14ad9aa-00e5-4ab1-8e04-6667e410e8ac
[1, 2, 3, 4]

# ╔═╡ f5cfc57c-c3a0-402e-b562-62a1384f137d
x(0) = 1
x(-1) = 2
x(-2) = 3
x(-3) = 4

[1, 4, 3, 2]

# ╔═╡ ddc9d9f4-d100-42e7-aa9d-041f92de0ba8
[1, 2, 3, 4, 5, 6]

# ╔═╡ 7c840621-a658-4095-aa24-788166ae7e3b
let
	ns = -10:10
	vec = 1:6
	function x(n)
		0<= n <= length(vec) - 1 && return vec[n+1]
		return 0
	end
	plot(
		stem(ns, x.(ns)),
		stem(ns, x.(-ns) .+ x.(-ns .- 6) .+ x.(-ns .+ 6) .+ x.(-ns .- 12) .+ x.(-ns .- 18) );
		layout=(2, 1)
		)
end

# ╔═╡ 3fd72b2f-2932-4590-b470-fe1f80677203
stem(0:5, [0, 2, 3, 0, -3, -2])

# ╔═╡ a1e462bb-6c2c-496d-b1b8-9f61f60d54ac
conv([1, 2, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0], [1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0])

# ╔═╡ e382f278-8b62-451b-afd5-40b861a3dd53
function ccirc(x, y)
	ifft(fft(x) .* fft(y))
end

# ╔═╡ a5fdebf8-c090-4973-9a56-b9532cc8d0ec
function ccirc_slow(x, y)
	N = length(x)
	@assert N == length(y)
	
	[ sum(x[k] * y[mod(n - k, N) + 1] for k in eachindex(x)) for n in eachindex(x) ]
end

# ╔═╡ c7cc647b-3d87-413a-879b-1583b34da901
let
	nlen = 10000
	@btime ccirc(rand($nlen), rand($nlen));
end

# ╔═╡ e1cd79ae-fe01-43f2-9bdf-d1c4f8cb300a
let
	nlen = 10000
	@btime ccirc_slow(rand($nlen), rand($nlen));
end

# ╔═╡ 7df30a6d-d209-45ce-8a1b-baa46994d18b
fft([1, 0, 0, 0, 1, 0, 0, 0])

# ╔═╡ b9fd320f-0d47-441e-9ec8-a43a088274b1
fft([1, 0, 0, 0, 1, 0, 0, 0])

# ╔═╡ 6aa7a838-e288-426f-9f61-4811e4232b6c
fftshift = DSP.fftshift;

# ╔═╡ 60558dcd-798f-4a6b-aabf-046597e8449e
fftshift([1, 2, 3, 4, 5])

# ╔═╡ a84d6de1-4a7d-4e88-950c-b09944e63347
let
	sr = 1000
	x = rand(3000)
	x .-= mean(x)
	
	fs = range(-sr/2; stop=sr/2, length=length(x))
	xf = fft(x)
	
	plot(fs, fftshift(abs.(xf)) ./ sr)
end

# ╔═╡ Cell order:
# ╠═51a38d90-c88c-11eb-3e4f-f59028e39b3e
# ╠═7a139c7b-6522-43d7-b517-6a9b49634366
# ╠═fd76c0e0-8b14-4814-9eeb-75e89ecea3b6
# ╠═dd0c98fa-d11d-41c4-9a96-139e2214b476
# ╟─c51feb27-1dbe-4a3d-9589-3b65e2e55759
# ╟─57b65e66-3b46-413a-8aac-0a1cadeda079
# ╟─f9419f95-e130-4dd6-985b-4c300e0cea7c
# ╟─eb5d1e48-2008-4dd2-87b2-7fe13ceaf642
# ╠═84137336-6e29-406d-a0d6-7b19850f0db7
# ╠═987ba14e-c430-4855-ac4f-1d11fc8c17ee
# ╠═ddf80887-0436-4cdf-8852-c638dea52608
# ╠═60558dcd-798f-4a6b-aabf-046597e8449e
# ╠═4ebdca91-f592-4278-9481-3c910d00258d
# ╠═cea79b39-8922-467b-834d-a60c6f106bda
# ╠═c14ad9aa-00e5-4ab1-8e04-6667e410e8ac
# ╠═f5cfc57c-c3a0-402e-b562-62a1384f137d
# ╠═ddc9d9f4-d100-42e7-aa9d-041f92de0ba8
# ╟─7c840621-a658-4095-aa24-788166ae7e3b
# ╠═3fd72b2f-2932-4590-b470-fe1f80677203
# ╠═a1e462bb-6c2c-496d-b1b8-9f61f60d54ac
# ╠═e382f278-8b62-451b-afd5-40b861a3dd53
# ╠═a5fdebf8-c090-4973-9a56-b9532cc8d0ec
# ╠═ca37313c-0e4d-4bcb-b436-f7970b92188e
# ╠═c7cc647b-3d87-413a-879b-1583b34da901
# ╠═e1cd79ae-fe01-43f2-9bdf-d1c4f8cb300a
# ╠═7df30a6d-d209-45ce-8a1b-baa46994d18b
# ╠═b9fd320f-0d47-441e-9ec8-a43a088274b1
# ╠═a84d6de1-4a7d-4e88-950c-b09944e63347
# ╟─6aa7a838-e288-426f-9f61-4811e4232b6c
