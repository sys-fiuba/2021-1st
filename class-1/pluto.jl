### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 88e8d94e-9c96-11eb-08f1-29ee4bc2e515
using SyS

# ╔═╡ 9e7a3ca5-fec7-4d32-8205-0b6ec109358a
md"""
Grafico el escalón
"""

# ╔═╡ 610001f2-3180-4cab-b741-1c0323cec968
stem(-5:10, u.(-5:10))

# ╔═╡ 386641f1-640a-440f-93c1-6a7c97e6d5cb
md"""
Grafico la delta discreta
"""

# ╔═╡ 8d10cbe0-f6df-4825-8295-930b0a998785
# Grafico la delta discreta
stem(-5:10, δ.(-10:10))

# ╔═╡ 4ef49ffe-cc64-4199-89fe-a27e4a125af5
md"""
Un slider para controlar el período de un coseno.
"""

# ╔═╡ 340dc74e-c1c5-4628-9dc6-ff2d5d0fecb0
@bind f0 Slider(0.2:0.01:4; default=1, show_value=true)

# ╔═╡ 3eb1eda8-45a8-4a2f-846c-afb341cee346
plot(t -> cos(2π * f0 * t), 0, 5)

# ╔═╡ b8d5f4de-130c-479e-8bea-23a612093153
md"""
# Serie geométrica

$\sum_{i=a}^b q^i = \frac{q^a - q^{b+1}}{1-q}$
"""

# ╔═╡ eb7d86eb-071d-498c-a7ec-93b44ff686df
seriegeom(q, a, b) = (q^a - q^(b+1)) / (1 - q)

# ╔═╡ e4b3be33-816b-4150-9ee0-bb35d35db2f6
seriegeom(2, 3, 4)

# ╔═╡ edd48f8c-96ef-4c69-9fcf-16e80fb7a6b0
2^3 + 2^4

# ╔═╡ d163af40-301e-47db-ad02-52778199b054
seriegeom(7, 2, 13)

# ╔═╡ 7bb4ab99-c662-404f-92e6-b31db9d3b7f4
sum( 7.0^i for i in 2:13 )

# ╔═╡ 799abeb7-ac0e-4c58-b059-a5f24ec7ea0c
md"""
# Cosas muy básicas de Julia
"""

# ╔═╡ 0651620c-7afb-4b72-a9c4-973203221ad9
exp(im * 2.4)

# ╔═╡ 69f82429-b063-4f45-a43c-30c79c327d40
cos(2.4) + im * sin(2.4)

# ╔═╡ c39ceedb-5416-4a14-ac93-6e4f66f0a6f1
# Un vector
[ 2, 3, 4, 4, 5 ]

# ╔═╡ 22eafc25-1f7e-4a56-ae53-bf9e2c0ce3f3
# Una matriz
[ 2 3; 4 5]

# ╔═╡ ad021841-5af2-4cf3-b5ee-101c1a2299ab
[ 2 3
  4 5 ]

# ╔═╡ 040eb05b-446f-4ba1-882b-12097f22aaae
0:0.5:10

# ╔═╡ 64d52acb-f2cb-47b1-bb64-95812e3e55d4
range(3; length=10, #= stop=34, length=3453 =#)

# ╔═╡ 6269e94c-7606-45d9-9253-8d1b53a3ab40
zeros(10)

# ╔═╡ 6916af8c-cc6a-4782-ad7e-54f8c8ff52b6
ones(10, 3)

# ╔═╡ 1af2480b-ebd2-4820-a55b-63dc9173e14f
# Número aleatorio uniforme entre 0 y 1
rand(5)

# ╔═╡ 16b43c4a-61bd-4cba-972e-47065a00bfc3
stem(rand(20))

# ╔═╡ 59f338a2-75f6-4ab4-8676-9003cfba4298
x = collect(11:20)

# ╔═╡ d8f8d0a7-2cfd-4f22-ba16-b695805195b9
x[5] = 34

# ╔═╡ 50317e2c-2fde-4276-92bd-ae87bde30e46
x[ [2, 3, 4] ]

# ╔═╡ 22213079-2c13-4b9e-b130-fbfdb2faeab4
x[2:5]

# ╔═╡ 7637898b-c7b2-4ec3-a249-166a96c1d59f
x[2:5] = [5, 23, -456, 234]

# ╔═╡ 05f83585-435e-490d-8309-506552b212a1
md"""
## Grafiquemos
"""

# ╔═╡ e0adbcf5-2061-4e05-afdd-b4bd3ce88e21
md"""
### Señales discretas
"""

# ╔═╡ 2ee3fea6-8a38-4347-ad50-10335df7a41e
stem( [10, 5.3, -2] )

# ╔═╡ c980e354-3d11-42c5-bc4f-2c9531bdd66d
stem(
	[0, 1, 2], 
	[10, 5.3, -2] 
)

# ╔═╡ a2b622a4-b33a-4e24-a90c-55425be6c02e
stem(vcat(zeros(10), ones(10)))

# ╔═╡ f7c90e42-a42b-4521-b5c6-990eeba79ba8
plot(vcat(zeros(10), ones(10)))

# ╔═╡ a04c8a17-d076-40e0-b71c-7d4de5048698
sin.( [ 2.3, 3.4 ] )

# ╔═╡ db40f504-3b3d-40b4-bf7b-6b4d5f935433
# Graficamos el escalón discreto
let
		# defino el eje horizontal
	ns = -20:25
		
		# Los valores
	xs = u.(ns)
	
	stem(ns, xs)
end

# ╔═╡ 8ea96b45-979b-46a4-91a1-27967c34f5c9
# Graficamos el escalón discreto
let
		# defino el eje horizontal
	ts = -20:0.01:25
		
		# Los valores
	xs = u.(ts)
	
	plot(ts, xs)
end

# ╔═╡ e822446a-2839-4b59-a1a6-4551353c37ac
plot( u, -2, 4 )

# ╔═╡ 0ba4552a-fb8a-45ca-813b-07c9f847c9bf
md"""
## Funciones
"""

# ╔═╡ 4910123f-2eef-4c04-8692-5a19222f037f
# "Lambda" functions
myfun = t -> sin(t)

# ╔═╡ 2ea65f86-5555-4128-b591-27325d2db97b
myfun2(t) = t^2 + 23

# ╔═╡ b36d4953-9966-4824-ac05-676c97fccbec
myfun2(34)

# ╔═╡ bf3d7a91-7dd6-4ee5-a621-1c9bde4a8120
md"""
# Loops y control
"""

# ╔═╡ 693b96e6-6ed8-4b97-baa6-be1b6f3c291b
if 2 < 3
	4
elseif 5 > 9
	8
else
	5
end

# ╔═╡ d2b2b2c6-c441-401a-9ad5-0400c6f5855f
2 < 3 ? 4 : 6

# ╔═╡ eb5d34e9-44b2-4a41-a421-38d996fb4d35
let
	i = 0
	while i < 4
		i += 1
	end
	i
end

# ╔═╡ 64704951-ae56-4cd2-8fd0-036b29d16274
let
	c = 0
	for x in [4, 7, 9]
		c += x
	end
	c
end

# ╔═╡ 1ef721d2-bc0a-4642-a6bb-8734e7fc642a
md"""
# Transformaciones en tiempo
"""

# ╔═╡ 779b6992-aae8-45c5-9b17-b6f08a66bc28
function x1(t)
	t < 0 && return 0
	t < 1 && return 1
	t < 2 && return 2 - t
	return 0
end

# ╔═╡ 087ece9a-0cc5-41c6-a8fc-229c0a997a32
plot(x1, -2, 5)

# ╔═╡ ff66d051-31f5-4c3e-be45-2aa3045fce4a
md"""
offset = $(@bind offset Slider(-3:0.1:3; show_value=true, default=0))
"""

# ╔═╡ fbf52069-47a1-4e56-867a-6123bbf81aa7
plot(
	t -> x1( t + offset ),
	-2, 
	5
)

# ╔═╡ a2cb69e4-f840-4ee6-854e-36588b273433
md"""
multiplico a t por... $(@bind kmult Slider(0.1:0.01:3; show_value=true, default=1))
"""

# ╔═╡ f784a577-2791-462a-b621-9e728ad78654
plot(
	t -> x1( kmult * t ),
	-2, 
	5
)

# ╔═╡ 86c8c681-94ea-4f55-a48f-9ba5dce821e7
plot(
	t -> x1( -t ),
	-2, 
	5
)

# ╔═╡ Cell order:
# ╠═88e8d94e-9c96-11eb-08f1-29ee4bc2e515
# ╠═9e7a3ca5-fec7-4d32-8205-0b6ec109358a
# ╠═610001f2-3180-4cab-b741-1c0323cec968
# ╠═386641f1-640a-440f-93c1-6a7c97e6d5cb
# ╠═8d10cbe0-f6df-4825-8295-930b0a998785
# ╟─4ef49ffe-cc64-4199-89fe-a27e4a125af5
# ╠═340dc74e-c1c5-4628-9dc6-ff2d5d0fecb0
# ╠═3eb1eda8-45a8-4a2f-846c-afb341cee346
# ╟─b8d5f4de-130c-479e-8bea-23a612093153
# ╠═eb7d86eb-071d-498c-a7ec-93b44ff686df
# ╠═e4b3be33-816b-4150-9ee0-bb35d35db2f6
# ╠═edd48f8c-96ef-4c69-9fcf-16e80fb7a6b0
# ╠═d163af40-301e-47db-ad02-52778199b054
# ╠═7bb4ab99-c662-404f-92e6-b31db9d3b7f4
# ╟─799abeb7-ac0e-4c58-b059-a5f24ec7ea0c
# ╠═0651620c-7afb-4b72-a9c4-973203221ad9
# ╠═69f82429-b063-4f45-a43c-30c79c327d40
# ╠═c39ceedb-5416-4a14-ac93-6e4f66f0a6f1
# ╠═22eafc25-1f7e-4a56-ae53-bf9e2c0ce3f3
# ╠═ad021841-5af2-4cf3-b5ee-101c1a2299ab
# ╠═040eb05b-446f-4ba1-882b-12097f22aaae
# ╠═64d52acb-f2cb-47b1-bb64-95812e3e55d4
# ╠═6269e94c-7606-45d9-9253-8d1b53a3ab40
# ╠═6916af8c-cc6a-4782-ad7e-54f8c8ff52b6
# ╠═1af2480b-ebd2-4820-a55b-63dc9173e14f
# ╠═16b43c4a-61bd-4cba-972e-47065a00bfc3
# ╠═59f338a2-75f6-4ab4-8676-9003cfba4298
# ╠═d8f8d0a7-2cfd-4f22-ba16-b695805195b9
# ╠═50317e2c-2fde-4276-92bd-ae87bde30e46
# ╠═22213079-2c13-4b9e-b130-fbfdb2faeab4
# ╠═7637898b-c7b2-4ec3-a249-166a96c1d59f
# ╟─05f83585-435e-490d-8309-506552b212a1
# ╟─e0adbcf5-2061-4e05-afdd-b4bd3ce88e21
# ╠═2ee3fea6-8a38-4347-ad50-10335df7a41e
# ╠═c980e354-3d11-42c5-bc4f-2c9531bdd66d
# ╠═a2b622a4-b33a-4e24-a90c-55425be6c02e
# ╠═f7c90e42-a42b-4521-b5c6-990eeba79ba8
# ╠═a04c8a17-d076-40e0-b71c-7d4de5048698
# ╠═db40f504-3b3d-40b4-bf7b-6b4d5f935433
# ╠═8ea96b45-979b-46a4-91a1-27967c34f5c9
# ╠═e822446a-2839-4b59-a1a6-4551353c37ac
# ╠═0ba4552a-fb8a-45ca-813b-07c9f847c9bf
# ╠═4910123f-2eef-4c04-8692-5a19222f037f
# ╠═2ea65f86-5555-4128-b591-27325d2db97b
# ╠═b36d4953-9966-4824-ac05-676c97fccbec
# ╟─bf3d7a91-7dd6-4ee5-a621-1c9bde4a8120
# ╠═693b96e6-6ed8-4b97-baa6-be1b6f3c291b
# ╠═d2b2b2c6-c441-401a-9ad5-0400c6f5855f
# ╠═eb5d34e9-44b2-4a41-a421-38d996fb4d35
# ╠═64704951-ae56-4cd2-8fd0-036b29d16274
# ╟─1ef721d2-bc0a-4642-a6bb-8734e7fc642a
# ╠═779b6992-aae8-45c5-9b17-b6f08a66bc28
# ╠═087ece9a-0cc5-41c6-a8fc-229c0a997a32
# ╠═fbf52069-47a1-4e56-867a-6123bbf81aa7
# ╟─ff66d051-31f5-4c3e-be45-2aa3045fce4a
# ╠═f784a577-2791-462a-b621-9e728ad78654
# ╟─a2cb69e4-f840-4ee6-854e-36588b273433
# ╠═86c8c681-94ea-4f55-a48f-9ba5dce821e7
