### A Pluto.jl notebook ###
# v0.15.0

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 7db058a4-cdf6-11eb-0895-1f6e5b376f3f
import Pkg; Pkg.activate(); using SyS

# ╔═╡ 940a7548-f62f-4121-9aa4-f5a9a8340e02
using SampledSignals, PortAudio

# ╔═╡ 7ce50fec-1788-415c-adf9-70b5ab3f128f
let 
  x = cos.(2π .* range(0; length=1024, step=0.1))
  x += 1e-2 .* rand(size(x)...)

  specplot(x; 
    overlap=0.9,
	onesided=true,
    window=rect(512), 
	fs=4)
end

# ╔═╡ 9bc8a200-2d4d-4729-a43f-064620d53bf6
let 
  x = cos.(2π .* range(0; length=1024, step=0.1))
  x += 1e-2 .* rand(size(x)...)

  specplot(x; 
    overlap=0.9, 
    window=hamming(256), onesided=false, fs=4)
end

# ╔═╡ f6e2769f-f648-4d48-91d5-537b4809c424
function x3(t)
	t < 10 && return cos(2π * 200 * t)
	
	return cos(2π * 100 * t)
end;

# ╔═╡ f7fdd1f4-68c8-4890-9848-e0951cf0b01b
fs3 = 1000;

# ╔═╡ accb4246-521e-4183-b125-d3895919bfaa
xd3 = x3.(range(0; stop=20, step=1/fs3));

# ╔═╡ c06c6ca9-e645-4d70-82d8-01bffc25b862
specplot(xd3;
	window=rect(2^5),
	nfft=2^10,
	fs3,
	clims=(-12, 6)
)

# ╔═╡ dc456d00-2daa-4f22-8e6e-0f0f9da9aca2
cos(polino)

# ╔═╡ ddcd76c6-5a36-4d36-a066-697549893df8
let
  fs = 10e3
  N  = 1e5
  amp = 2 * sqrt(2)
  noise_power = 0.04 * fs / 2
  time = (0:N-1) ./ fs
  mod = 1000 .* cos.(2pi * 0.25 .* time) # moduladora
  carrier = amp .* sin.(2pi * 3e3 .* time .+ mod)
  noise = randn(size(time)) .* sqrt(noise_power)
  noise .*= exp.(-time ./ 5)
  x = carrier .+ noise
  
  specplot(x; 
    overlap=0.9, 
    window=tukey(256, 0.5), 
	clims=(2, 6),
    fs=fs)
end

# ╔═╡ 67076162-9f64-4b1e-9489-fc56c77aaae9
@bind rec Button("Rec")

# ╔═╡ 99d3d27a-8b4f-4336-8993-aa551f1ccb03
buf = PortAudioStream(1, 0) do str
		rec
		read(str.source, 5s)
end

# ╔═╡ 3b32e1a8-2499-4e4a-80d8-565a37c186d9
with(:plotly) do
plot(domain(buf), buf;
	xlabel="t",
	legend=false
	)
end

# ╔═╡ 38f59e00-6e65-425d-b20d-b8ab8566baf1
with(:plotly) do
	xf = fft(buf)
	plot(
		domain(xf),
		abs.(xf);
		xlims=(0, 8e3),
		yticks=nothing,
		legend=false,
		title="Espectro 👻",
		xlabel="FRECUENCIA (Hz)"
	)
end

# ╔═╡ 23e32795-b0da-461c-8c8c-c5a19f602e84
md"""
# Cargar un WAV
"""

# ╔═╡ abb450f6-67ba-4f9d-b24e-4a435138db99
function loadwav(fn::String)
	data, sr, = wavread(fn)
	data = mean(data; dims=2)[:]
	
	Float64.(data), Float64(sr)
end;

# ╔═╡ 61537cf2-948c-473c-84ea-6cd879286a60
x, sr = loadwav("/home/rui/celine.wav");

# ╔═╡ 28c65c1c-49de-4aa4-b81b-e22f7b2b86ee
wavwrite(xnew, "Shtaoshetaonseht.wav"; Fs=sr)

# ╔═╡ d9b691a1-f30e-4c48-b323-102133bbe316
length(x) / sr / 60

# ╔═╡ fb33e323-2b25-4f42-aa81-9d7f52c7a728
plotly()

# ╔═╡ 96f4e5a9-dc27-4ee8-9051-7d359c8d1661
specplot(x[1000000:1200000]; window=hanning(2^10), nfft=2^11, fs=sr, ylims=(0, 10e3))

# ╔═╡ bbaee918-bd16-44bf-8d4d-9a3d5fede061
begin
	xrec = collect(vec(buf))
	fs = samplerate(buf)
end;

# ╔═╡ 6a8b093b-07d4-4871-a91a-565d58a3d33c
specplot(xrec; fs, ylims=(0, 3e3), nfft = 2^12, window=hanning(2^11))

# ╔═╡ Cell order:
# ╠═7db058a4-cdf6-11eb-0895-1f6e5b376f3f
# ╠═7ce50fec-1788-415c-adf9-70b5ab3f128f
# ╠═9bc8a200-2d4d-4729-a43f-064620d53bf6
# ╠═f6e2769f-f648-4d48-91d5-537b4809c424
# ╠═f7fdd1f4-68c8-4890-9848-e0951cf0b01b
# ╠═accb4246-521e-4183-b125-d3895919bfaa
# ╠═c06c6ca9-e645-4d70-82d8-01bffc25b862
# ╠═dc456d00-2daa-4f22-8e6e-0f0f9da9aca2
# ╠═ddcd76c6-5a36-4d36-a066-697549893df8
# ╠═940a7548-f62f-4121-9aa4-f5a9a8340e02
# ╠═67076162-9f64-4b1e-9489-fc56c77aaae9
# ╠═99d3d27a-8b4f-4336-8993-aa551f1ccb03
# ╟─3b32e1a8-2499-4e4a-80d8-565a37c186d9
# ╟─38f59e00-6e65-425d-b20d-b8ab8566baf1
# ╟─6a8b093b-07d4-4871-a91a-565d58a3d33c
# ╟─23e32795-b0da-461c-8c8c-c5a19f602e84
# ╠═abb450f6-67ba-4f9d-b24e-4a435138db99
# ╠═28c65c1c-49de-4aa4-b81b-e22f7b2b86ee
# ╠═61537cf2-948c-473c-84ea-6cd879286a60
# ╠═d9b691a1-f30e-4c48-b323-102133bbe316
# ╠═fb33e323-2b25-4f42-aa81-9d7f52c7a728
# ╠═96f4e5a9-dc27-4ee8-9051-7d359c8d1661
# ╟─bbaee918-bd16-44bf-8d4d-9a3d5fede061
