### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 231fce02-ad05-11eb-1be9-e5d4112fee76
using SyS

# ╔═╡ e6ef157d-55ad-472e-a408-1fd48f8d3cbf
md"""
$a_k = 1/N \sum_{n=0}^{N-1} x(n) e^{-j 2 π k n / N}$
"""

# ╔═╡ dcf50c45-f526-4600-abb8-6acca7447764
md"""
$x(n) = \sum_{k=0}^{N-1} a_k e^{j 2 π k n / N}$
"""

# ╔═╡ 1bd430b8-b1a5-445e-832e-960017fb1402
function coef_serie(x)
	N = length(x)
	out = zeros(ComplexF64, N)
	
	for k in 0:N-1
		kidx = k + 1
		for n in 0:N-1
			nidx = n + 1
			
			out[kidx] += 1/N * x[nidx] * exp(-im * 2π * k * nidx / N)
		end
	end
	return out
end

# ╔═╡ ad79ae9e-58fb-41af-9b4c-f701ffbe504a
function coef_serie2(x)
	N = length(x)
	
	[ 
		sum( x[n+1] * exp(-im * 2π * k * n / N) 
			for n in 0:N-1)
		for k in 0:N-1	
	] ./ N
end

# ╔═╡ 7b8220e5-6f1a-40f7-8b7f-e52631c02ae9
function synth(a)
	N = length(a)
	
	[ 
		sum( a[k+1] * exp(im * 2π * k * n / N) 
			for k in 0:N-1)
		for n in 0:N-1	
	]
end

# ╔═╡ 72077a38-1af2-4e74-a9c6-7c76230138ff
Diagonal([2, 3, 5])

# ╔═╡ dd6704b7-c0c3-4f2b-b8bf-eeca2d6319ab
base(N) = [ exp(im * 2π * k * n / N) for n in 0:N-1, k in 0:N-1]

# ╔═╡ 6c487e07-4a93-4983-9e58-aa28be7b265c
let
	ns = -10:20
	x(n) = u(n) - u(n - 3)
	
	stem(ns, x.(ns))
	
end

# ╔═╡ 2e28e4d1-d44b-412f-b7ac-19456728b160
x2(n) = sin(π * n / 4)

# ╔═╡ 652b4f44-9e5f-48eb-9678-9c388e7d7aec
function cuadrada(n)
	0 <= n < 4 && return -1
	4 <= n < 7 && return 1
end

# ╔═╡ 8e5e8ac8-55e4-416f-a2ab-0a388ac8a14f
xtot(n) = x2(n) * cuadrada(n) + 1

# ╔═╡ ff7a4161-365e-4244-801e-861d25474fbc
let
	ns = 0:7
	stem( ns, xtot.(ns))
	#stem!( ns, laposta.(ns))
end

# ╔═╡ 8ed4a9d4-561a-47ff-9710-5412c29bc5e3
laposta(n) = 1 - sin(π * n / 4)

# ╔═╡ Cell order:
# ╠═231fce02-ad05-11eb-1be9-e5d4112fee76
# ╠═e6ef157d-55ad-472e-a408-1fd48f8d3cbf
# ╠═dcf50c45-f526-4600-abb8-6acca7447764
# ╠═1bd430b8-b1a5-445e-832e-960017fb1402
# ╠═ad79ae9e-58fb-41af-9b4c-f701ffbe504a
# ╠═7b8220e5-6f1a-40f7-8b7f-e52631c02ae9
# ╠═72077a38-1af2-4e74-a9c6-7c76230138ff
# ╠═dd6704b7-c0c3-4f2b-b8bf-eeca2d6319ab
# ╠═6c487e07-4a93-4983-9e58-aa28be7b265c
# ╠═2e28e4d1-d44b-412f-b7ac-19456728b160
# ╠═652b4f44-9e5f-48eb-9678-9c388e7d7aec
# ╠═8e5e8ac8-55e4-416f-a2ab-0a388ac8a14f
# ╠═ff7a4161-365e-4244-801e-861d25474fbc
# ╠═8ed4a9d4-561a-47ff-9710-5412c29bc5e3
