### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 21d8e2ba-a1fc-11eb-2956-8388676f7cd5
using SyS

# ╔═╡ 365fb70f-fcb5-4c96-b94c-9269c5849b25
3^2

# ╔═╡ 9619d1ee-4fa1-4566-9aa3-046981a08464
let
	ns = -4:4
	xa(n) = -2δ(n + 2)
	xb(n) = 2^float(n) * u(n) 
	
	stem(ns, xb.(ns))
end

# ╔═╡ a464d1b5-17db-47b2-a7ac-a98198dd3d7d
let
	N = 10
	ns = -4:14
	xs = vcat( zeros(4), ones(10), zeros(5) )
	#x(n) = u(n) - u(n - N)
	
	stem(ns, xs)
end

# ╔═╡ 77006a39-17ec-411c-8b90-e7b0915fc60f
let
	ns = -7:8
	xs = [0, 0, 0, 0, 0, 1, 2, 3, 2, 2, 1, 0, 0, 0, 0, 0]
	#x(n) = u(n) - u(n - N)
	
	stem(ns, xs)
end

# ╔═╡ 9a6c3877-3efd-4cbc-86e9-b6adb884c468
begin
	plot(t -> cos(2pi*t), 0, 3)
	plot!(t -> cos(2*2pi*t), 0, 3)
end

# ╔═╡ 15c9ac49-8a05-4052-9ed2-2495f870f12a
# y1([2, 3, 4, 5, 7, 13 ] ) --> [2, 4, 7]
y1(x) = x[1:2:end]

# ╔═╡ 48d6e5ee-aefa-441e-8640-c204a7dd839e
let
	ns = -5:10
	x(n) = n #=cos(2pi/7 * n)=# * (u(n) - u(n - 8))
	y1(n) = x(2n)
	
	stem(ns, x.(ns))
	
	stem!(ns, y1.(ns))
end

# ╔═╡ 1515de1c-ae6a-4080-a3d0-a821857dcd70
# y1([2, 3, 4, 5, 7, 13 ] ) --> [2, 0, 3,  0, 4, 0, 5, 0, 7, 0, 13, 0]
function y2(x)
	out = zeros(eltype(x), 2length(x))
	
	out[ 1:2:end ] .= x
	
	return out
end

# ╔═╡ a5e07b8d-e3a2-4044-a763-61bfefad9c26
begin
	T = sqrt(2)
	xs = t -> cos(2pi / T * t)
	
	plot(xs, 0, 6)
	
	Ts = 1
	ns = 0:Ts:6
	
	stem!(ns, xs.(ns))
end

# ╔═╡ Cell order:
# ╠═21d8e2ba-a1fc-11eb-2956-8388676f7cd5
# ╠═365fb70f-fcb5-4c96-b94c-9269c5849b25
# ╠═9619d1ee-4fa1-4566-9aa3-046981a08464
# ╠═a464d1b5-17db-47b2-a7ac-a98198dd3d7d
# ╠═77006a39-17ec-411c-8b90-e7b0915fc60f
# ╠═9a6c3877-3efd-4cbc-86e9-b6adb884c468
# ╠═48d6e5ee-aefa-441e-8640-c204a7dd839e
# ╠═15c9ac49-8a05-4052-9ed2-2495f870f12a
# ╠═1515de1c-ae6a-4080-a3d0-a821857dcd70
# ╠═a5e07b8d-e3a2-4044-a763-61bfefad9c26
